import { StyleSheet } from "react-native";

export default GlobalStyles = StyleSheet.create({
  headerContainer: {
    height: 50,
    justifyContent: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  titleContainer: {
    alignItems: "center",
  },
  titleText: {
    fontSize: 30,
    // borderBottomWidth: 1,
    // borderColor: "orange",
    // paddingHorizontal: 30,
  },
});
