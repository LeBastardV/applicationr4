import React, { useEffect } from "react";
import { Text, StyleSheet, View, SafeAreaView, StatusBar } from "react-native";
import Header from "../shared/Header";
import { _storeAllArtists, _clearAsyncStorage } from "../data/AsyncStorage";

export default function HomeScreen({ navigation }) {
  data = async () => {
    const test = await _storeAllArtists();
  };
  useEffect(() => {
    data();
  }, []);
  return (
    <SafeAreaView style={styles.container}>
      <Header navigation={navigation} />
      <View style={styles.text_containter}>
        <Text>HOME PAGE IN COMING</Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text_containter: {
    alignItems: "center",
  },
});
