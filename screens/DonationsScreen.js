import React from "react";
import { Text, View, StyleSheet, SafeAreaView } from "react-native";
import Header from "../shared/Header";

export default DonationScreen = ({ navigation }) => (
  <SafeAreaView style={styles.container}>
    <Header navigation={navigation} />
    <View style={styles.text_containter}>
      <Text> Donation page in coming </Text>
      <Text> Integration du formulaire de don </Text>
      <Text> A determiner comment faire </Text>
    </View>
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text_containter: {
    alignItems: "center",
  },
});
