import React from "react";
import { Text, View, StyleSheet, SafeAreaView } from "react-native";
import Header from "../shared/Header";

export default SocialNetworkScreen = ({ navigation }) => (
  <SafeAreaView style={styles.container}>
    <Header navigation={navigation} />
    <View style={styles.text_containter}>
      <Text> Social network page in coming </Text>
      <Text> integreation de Facebook et le playliste Souncloud </Text>
    </View>
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text_containter: {
    alignItems: "center",
  },
});
