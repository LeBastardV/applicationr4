import React from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import HeaderGoBack from "../shared/HeaderGoBack";
import { MaterialIcons, Zocial } from "@expo/vector-icons";
import { useSelector, useDispatch } from "react-redux";
import { toggleFavorite } from "../store/actions";

export default ArtistsDetail = ({ route, navigation }) => {
  const image_path = () =>
    "https://2019.festivalr4.fr/img/groupes/" +
    route.params.image_path +
    "/image.jpg";

  const favoritesArtists = useSelector((state) => state.favoriteArtists);
  const dispatch = useDispatch();

  const LOGO = _displayFavoriteIcon();

  function _displayFavoriteIcon() {
    if (
      favoritesArtists.findIndex((item) => item.id === route.params.id) !== -1
    ) {
      return "favorite";
    }
    return "favorite-border";
  }

  return (
    <SafeAreaView style={styles.main_container}>
      <View style={styles.header_container}>
        <HeaderGoBack navigation={navigation} />
        <Text adjustsFontSizeToFit minimumFontScale={1.7}>
          {route.params.name}
        </Text>
        <TouchableOpacity>
          <MaterialIcons
            name={LOGO}
            size={25}
            style={{
              marginRight: 10,
              paddingHorizontal: 5,
              color: "#000",
            }}
            onPress={() => {
              dispatch(toggleFavorite(route.params));
            }}
          />
        </TouchableOpacity>
      </View>
      <Image style={styles.image} source={{ uri: image_path() }} />
      <View style={styles.date_container}>
        <Text>{route.params.stage} </Text>
        <Text>{route.params.day} </Text>
        <Text>{route.params.hour} </Text>
      </View>

      <ScrollView style={styles.description_container}>
        <Text style={styles.description_text}>{route.params.overview} </Text>
      </ScrollView>

      <TouchableOpacity>
        <View style={styles.date_container}>
          <Text>Voir {route.params.name} sur le web </Text>
        </View>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header_container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  image: {
    width: "100%",
    height: 250,
    resizeMode: "cover",
  },
  date_container: {
    flexDirection: "row",
    backgroundColor: "#6abfbaEE",
    height: 40,
    marginHorizontal: 20,
    marginVertical: 10,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  description_container: {
    flex: 1,
    marginHorizontal: 20,
  },
  description_text: {
    fontSize: 18,
    textAlign: "center",
  },
  socialN_container: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginVertical: 30,
    justifyContent: "space-around",
  },
  icon: { color: "#6abfba", marginBottom: 10 },
});
