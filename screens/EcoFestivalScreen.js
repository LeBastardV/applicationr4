import React from "react";
import { Text, View, StyleSheet, FlatList, SafeAreaView } from "react-native";
import Header from "../shared/Header";
import EcoItem from "../components/EcoItem";
import EcoData from "../data/EcoData";

export default EcoFestivalScreen = ({ navigation }) => (
  <SafeAreaView style={styles.main_container}>
    {/* Header */}
    <View style={styles.header_container}>
      <Header navigation={navigation} />
      <Text>R4, L'ECO-FESTIVAL</Text>
      <View style={{ width: 38 }}></View>
    </View>
    {/* Content */}
    <FlatList
      data={EcoData}
      keyExtractor={(item) => item.id.toString()}
      renderItem={({ item }) => <EcoItem data={item} />}
    />
  </SafeAreaView>
);

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header_container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
});
