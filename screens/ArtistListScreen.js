import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  TextInput,
  TouchableOpacity,
  FlatList,
  View,
  SafeAreaView,
  ActivityIndicator,
} from "react-native";
import ArtistItem from "../components/ArtistsItem";
import Header from "../shared/Header";
import { MaterialIcons } from "@expo/vector-icons";
import GlobalStyles from "../styles/GlobalStyle";

import { _getAllArtists, _clearAsyncStorage } from "../data/AsyncStorage";

export default function ArtistListScreen({ navigation }) {
  const [dataSource, setdataSource] = useState([]);
  const [isLoading, setisLoading] = useState(true);
  const openMenu = () => {
    navigation.openDrawer();
  };

  getData = async () => {
    const dataArtists = await _getAllArtists();
    setdataSource(dataArtists);
    setisLoading(false);
  };

  //import data from json serverex
  useEffect(() => {
    getData();
  }, []);

  if (isLoading) {
    return (
      <SafeAreaView style={styles.main_container}>
        <Header navigation={navigation} />
        <View style={styles.main_container}>
          <ActivityIndicator size="large" animating />
        </View>
      </SafeAreaView>
    );
  } else {
    return (
      <SafeAreaView style={styles.main_container}>
        <View style={GlobalStyles.headerContainer}>
          <Header navigation={navigation} />
          <TextInput
            style={styles.searchBar}
            placeholder="Artiste / Groupe"
            onChangeText={(text) => this._searchTextInputChanged(text)}
            onSubmitEditing={() => this._loadFilms()}
          />
          <TouchableOpacity>
            <MaterialIcons
              name="search"
              size={28}
              style={{
                marginRight: 10,
                paddingHorizontal: 5,
                color: "#000",
              }}
              onPress={() => {}}
            />
          </TouchableOpacity>
        </View>
        <FlatList
          data={dataSource}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <ArtistItem artist={item} navigation={navigation} />
          )}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
  },
  searchBar: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    height: 30,
    borderColor: "#000000",
    borderWidth: 1,
    paddingLeft: 5,
    borderRadius: 10,
  },
  searchbar_container: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
