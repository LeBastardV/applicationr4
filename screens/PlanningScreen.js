import React from "react";
import { Text, View, StyleSheet, SafeAreaView } from "react-native";
import Header from "../shared/Header";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import Vendredi from "./planningScreens/Vendredi";
import Samedi from "./planningScreens/Samedi";
import Dimanche from "./planningScreens/Dimanche";

export default PlanningScreen = ({ navigation }) => {
  const Tab = createBottomTabNavigator();

  return (
    <SafeAreaView style={styles.container}>
      <Header navigation={navigation} />
      <View style={styles.text_containter}>
        <Text>Planning PAGE IN COMING</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text_containter: {
    alignItems: "center",
  },
});
