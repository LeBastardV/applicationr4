import React from "react";
import { Text, View, Image, StyleSheet, SafeAreaView } from "react-native";
import Header from "../shared/Header";

export default AboutScreen = ({ navigation }) => (
  <SafeAreaView style={styles.container}>
    <Header navigation={navigation} />
    <View style={styles.logoContainer}>
      <Image
        style={styles.logo}
        source={require("../data/images/logo-R4-noir.png")}
      />
      <Text style={styles.textName}>
        Association Rencontres Rock'N'Roll à Revelles (R4)
      </Text>
    </View>
    <Text style={styles.text}>
      Application réalisée exclusivement pour le Festival R4
    </Text>
  </SafeAreaView>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  logoContainer: {
    alignItems: "center",
    borderBottomColor: "black",
    borderBottomWidth: 1,
    marginHorizontal: 20,
  },
  logo: {
    width: 250,
    height: 250,
  },
  textContainer: {
    flex: 3,
  },
  text: {
    paddingVertical: 20,
    marginHorizontal: 30,
    textAlign: "center",
  },
  textName: {
    paddingVertical: 20,
    marginHorizontal: 30,
    textAlign: "center",
    fontStyle: "italic",
  },
});
