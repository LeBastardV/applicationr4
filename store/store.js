import { createStore } from "redux";
import { toggleFavoriteArtists } from "./reducers";

export default store = createStore(toggleFavoriteArtists);
