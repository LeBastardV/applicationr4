import { TOGGLE_FAVORITE } from "./actionsTypes";

const initialState = { favoriteArtists: [] };

export const toggleFavoriteArtists = (state = initialState, action) => {
  let nextState;
  switch (action.type) {
    case TOGGLE_FAVORITE:
      const favoriteArtistsIndex = state.favoriteArtists.findIndex(
        (item) => item.id === action.value.id
      );
      if (favoriteArtistsIndex !== -1) {
        // L'artiste est déjà dans les favoris, on le supprime de la liste
        nextState = {
          ...state,
          favoriteArtists: state.favoriteArtists.filter(
            (item, index) => index !== favoriteArtistsIndex
          ),
        };
      } else {
        // L'artiste n'est pas dans les favoris, on l'ajoute à la liste
        nextState = {
          ...state,
          favoriteArtists: [...state.favoriteArtists, action.value],
        };
      }
      return nextState || state;
    default:
      return state;
  }
};
