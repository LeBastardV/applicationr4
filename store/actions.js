import { TOGGLE_FAVORITE } from "../store/actionsTypes";

export const toggleFavorite = (VALUE) => ({
  type: TOGGLE_FAVORITE,
  value: VALUE,
});
