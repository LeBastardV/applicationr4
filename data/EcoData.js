export default data = [
  {
    id: 1,
    title: "Circuit court",
    content:
      "L'organisation du Festival Rock R4 fait fonctionner le commerce de proximité. 90 % de nos achats sont réalisés dans un périmètre de 30 kilomètres autour du festival.",
    quote: "100% de fournisseurs français - sélection de bières régionales",
    image_path: require("../data/images/ecoIcon/circuit-court.png"),
  },
  {
    id: 2,
    title: "Gobelets reutilisables Festival Rock R4",
    content:
      "Les verres réutilisables, du fait de leur « durabilité », contribuent à préserver l’environnement et à réduire la production de déchets à la source. Grâce à ce système, il n’est plus nécessaire de produire une multitude de verres jetables.",
    quote: "Passer d’un bien jetable à un bien durable.",
    image_path: require("../data/images/ecoIcon/gobelets.png"),
  },
  {
    id: 3,
    title: "Des toilettes seches!",
    content:
      "Les toilettes sèches sont des toilettes qui n’utilisent pas d’eau. Il est donc possible de récupérer les excréments pour en faire du compost ou de la biométhanisation. Des toilettes sèches d’abord utilisées pour les refuges et zones isolées ont été adaptées pour des festivals regroupant plusieurs dizaines de milliers de visiteurs, et y ont rencontré de franc succès. Il suffit de prévoir des panneaux explicatifs, l’entretien nécessaire et un peu de surveillance. Elles deviennent ainsi une alternative raisonnable aux toilettes chimiques.",
    quote:
      "Peu onéreux • Facile à mettre en œuvre • Ne pollue pas les cours d’eau",
    image_path: require("../data/images/ecoIcon/toilettes-seches.png"),
  },
  {
    id: 4,
    title: "Centre de tri selectif",
    content:
      "Le tri sélectif des déchets et la collecte sélective sont des actions consistant à séparer et récupérer les déchets selon leur nature, à la source, pour éviter les contacts et les souillures. Ceci permet de leur donner une « seconde vie », le plus souvent par le réemploi et le recyclage évitant ainsi leur simple et brutale destruction par incinération ou abandon en décharge.",
    quote: "Réduction de l'empreinte écologique",
    image_path: require("../data/images/ecoIcon/recyclage.png"),
  },
  {
    id: 5,
    title: "Communication",
    content:
      "Tous les supports de communication du festival sont imprimés sur du papier recyclé et ce, par des imprimeurs labellisés Imprim’Vert.",
    quote: "Réduction de l'empreinte écologique",
    image_path: require("../data/images/ecoIcon/imprimvert.png"),
  },
];
