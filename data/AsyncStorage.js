import { AsyncStorage } from "react-native";

async function _fetchData(URL) {
  let data = [];
  await fetch(URL).then((response) => (data = response.json()));
  return data;
}

async function _storeJson(key, json) {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(json));
  } catch (error) {
    console.log("Erreur", error);
  }
}

async function _retrieveData(key) {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      let val = JSON.parse(value);
      return val;
    }
  } catch (error) {
    console.log("Erreur", error);
  }
}

export async function _clearAsyncStorage() {
  AsyncStorage.clear();
  console.log("Async storage cleared");
}

export async function _storeAllArtists() {
  try {
    let fetchedData = await _fetchData(
      "https://my-json-server.typicode.com/LeBastardV/r4festivaldata/db"
    );
    let storedVersion = await _retrieveData("version");
    if (
      storedVersion == null ||
      storedVersion.value != fetchedData.version.value
    ) {
      console.log("Different version");
      //Store data
      await _storeJson("version", fetchedData.version);
      console.log("Version Updated");

      //Update version
      await _storeJson("artists", fetchedData.artists);
      console.log("Artists Updated");
    } else {
      console.log("Same version");
    }
  } catch (error) {
    console.log(error);
  }
}

export async function _getAllArtists() {
  let allArtists = await _retrieveData("artists");
  return allArtists;
}
