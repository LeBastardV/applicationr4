import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";

export default EcoItem = ({ data }) => {
  return (
    <View style={styles.main_container}>
      <View style={styles.tile_container}>
        <Image style={styles.image} source={data.image_path} />
        <Text style={styles.title_text}>{data.title}</Text>
      </View>
      <View>
        <Text>{data.content}</Text>
        <Text style={styles.description_text}>{data.quote}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    alignItems: "center",
    marginHorizontal: 10,
    borderColor: "#6abfba",
    borderBottomWidth: 1,
    padding: 5,
  },
  image: {
    width: 50,
    height: 50,
    margin: 5,
  },
  tile_container: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  title_text: {
    fontSize: 22,
    flexWrap: "wrap",
    alignItems: "center",
    flex: 1,
    textAlign: "center",
  },
  description_container: {},
  description_text: {
    fontStyle: "italic",
  },
});
