import React from "react";
import { StyleSheet, View, Text, Image, TouchableOpacity } from "react-native";

export default ArtistItem = ({ artist, navigation }) => {
  const pressDetailArtists = () => navigation.navigate("ArtistDetail", artist);
  const image_path = () =>
    "https://2019.festivalr4.fr/img/groupes/" +
    artist.image_path +
    "/image.jpg";
  return (
    <TouchableOpacity
      style={styles.main_container}
      onPress={pressDetailArtists}
    >
      <Image style={styles.image} source={{ uri: image_path() }} />
      <View style={styles.content_container}>
        <View style={styles.header_container}>
          <Text style={styles.name_text}>{artist.name}</Text>
        </View>

        <View style={styles.description_container}>
          <Text style={styles.description_text} numberOfLines={3}>
            {artist.overview}
          </Text>
        </View>

        <View style={styles.date_container}>
          <Text style={styles.date_text}>{artist.day} </Text>
          <Text style={styles.date_text}> {artist.hour} </Text>
          <Text style={styles.date_text}>: {artist.stage} </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main_container: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 5,
    marginHorizontal: 10,
    borderRadius: 10,
    borderColor: "#6abfba",
    borderWidth: 2,
    borderStyle: "dashed",
    padding: 3,
  },
  image: {
    width: 120,
    height: 120,
    margin: 5,
    transform: [{ rotate: "5deg" }],
  },
  content_container: {
    height: 120,
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "column",
    margin: 5,
  },
  header_container: {
    flexDirection: "row",
  },
  name_text: {
    fontWeight: "bold",
    fontSize: 13,
    paddingRight: 5,
  },
  description_container: {},
  description_text: {
    fontStyle: "italic",
  },
  date_container: {
    flexDirection: "row",
  },
  date_text: {
    textAlign: "right",
    fontSize: 13,
  },
});
