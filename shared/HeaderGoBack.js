import React from "react";
import { View, TouchableOpacity, StatusBar } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import GlobalStyles from "../styles/GlobalStyle";

const MenuButton = ({ navigation }) => {
  const goBack = () => {
    navigation.goBack();
  };
  return (
    <TouchableOpacity>
      <MaterialIcons
        name="arrow-back"
        size={28}
        style={{
          marginLeft: 10,
          paddingHorizontal: 5,
          color: "#000",
        }}
        onPress={goBack}
      />
    </TouchableOpacity>
  );
};

export default Header = ({ navigation }) => (
  <View style={GlobalStyles.headerContainer}>
    <MenuButton navigation={navigation} />
  </View>
);
