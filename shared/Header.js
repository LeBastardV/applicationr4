import React from "react";
import { TouchableOpacity, SafeAreaView, StatusBar } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import GlobalStyles from "../styles/GlobalStyle";

const MenuButton = ({ navigation }) => {
  const openMenu = () => {
    navigation.openDrawer();
  };
  return (
    <TouchableOpacity>
      <MaterialIcons
        name="menu"
        size={28}
        style={{
          marginLeft: 10,
          paddingHorizontal: 5,
          color: "#000",
        }}
        onPress={openMenu}
      />
    </TouchableOpacity>
  );
};

export default Header = ({ navigation }) => (
  <SafeAreaView style={GlobalStyles.headerContainer}>
    <StatusBar />
    <MenuButton navigation={navigation} />
  </SafeAreaView>
);
