import React from "react";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
} from "@react-navigation/drawer";
import { createStackNavigator } from "@react-navigation/stack";
import { TouchableOpacity, View, Image } from "react-native";
import { MaterialIcons, FontAwesome5 } from "@expo/vector-icons";

//import screens
import HomeScreen from "../screens/HomeScreen";
import ArtistListScreen from "../screens/ArtistListScreen";
import ArtistDetailScreen from "../screens/ArtistsDetail";
import AboutScreen from "../screens/AboutScreen";
import EcoFestivalScreen from "../screens/EcoFestivalScreen";
import DonationsScreen from "../screens/DonationsScreen";
import PlanningScreen from "../screens/PlanningScreen";
import SocialNetworkScreen from "../screens/SocialNetworkScreen";

const Drawer = createDrawerNavigator();
const ArtistListStack = createStackNavigator();

const MenuButton = ({ navigation }) => {
  const openMenu = () => {
    navigation.openDrawer();
  };
  return (
    <TouchableOpacity>
      <MaterialIcons
        name="menu"
        size={28}
        style={{
          marginLeft: 10,
          marginTop: 5,
          paddingHorizontal: 5,
          color: "#000",
        }}
        onPress={openMenu}
      />
    </TouchableOpacity>
  );
};

const ArtistListStackRouter = ({ navigation }) => {
  return (
    <ArtistListStack.Navigator headerMode="none">
      <ArtistListStack.Screen name="ArtistList" component={ArtistListScreen} />
      <ArtistListStack.Screen
        name="ArtistDetail"
        component={ArtistDetailScreen}
      />
    </ArtistListStack.Navigator>
  );
};

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>
      <View
        style={{
          flex: 1,
          alignItems: "center",
          borderBottomColor: "#6abfba",
          borderBottomWidth: 2,
          marginHorizontal: 20,
          paddingBottom: 10,
          marginVertical: 10,
        }}
      >
        <Image
          style={{ height: 150, width: 150 }}
          source={require("../data/images/logo-R4-noir.png")}
        />
      </View>

      <DrawerItemList {...props} />
    </DrawerContentScrollView>
  );
}

export default DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}
      drawerType="slide"
      options={{ size: 55 }}
      drawerContentOptions={{
        activeTintColor: "#6abfba",
        itemStyle: { marginVertical: 5, marginHorizontal: 15 },
      }}
    >
      <Drawer.Screen
        name="Accueil"
        component={HomeScreen}
        options={{
          drawerIcon: () => <FontAwesome5 name="home" size={18} color="#000" />,
        }}
      />
      <Drawer.Screen
        name="Artistes"
        component={ArtistListStackRouter}
        options={{
          drawerIcon: () => <FontAwesome5 name="user" size={18} color="#000" />,
        }}
      />
      <Drawer.Screen
        name="Eco-Festival"
        component={EcoFestivalScreen}
        options={{
          drawerIcon: () => (
            <FontAwesome5 name="recycle" size={18} color="#000" />
          ),
        }}
      />
      <Drawer.Screen
        name="Réseaux sociaux"
        component={SocialNetworkScreen}
        options={{
          drawerIcon: () => (
            <FontAwesome5 name="share-alt" size={18} color="#000" />
          ),
        }}
      />
      <Drawer.Screen
        name="Programmation"
        component={PlanningScreen}
        options={{
          drawerIcon: () => (
            <FontAwesome5 name="table" size={18} color="#000" />
          ),
        }}
      />
      <Drawer.Screen
        name="Faire un don"
        component={DonationsScreen}
        options={{
          drawerIcon: () => (
            <FontAwesome5 name="heart" size={18} color="#000" />
          ),
        }}
      />
      <Drawer.Screen
        name="A propos"
        component={AboutScreen}
        options={{
          drawerIcon: () => (
            <FontAwesome5 name="copyright" size={18} color="#000" />
          ),
        }}
      />
    </Drawer.Navigator>
  );
};
